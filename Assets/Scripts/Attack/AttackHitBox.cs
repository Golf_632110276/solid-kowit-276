using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    public GameObject Host;
    [SerializeField] private Player player;
    [SerializeField] private int damage;

    private void Start()
    {
        AttackController.OnChangedWeapon += OnWeaponChanged;
        this.gameObject.SetActive(false);
    }

    public void OnWeaponChanged(WeaponData _weaponData)
    {
        if (_weaponData == null) return;
        damage = _weaponData.Damage;
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.gameObject == Host) return;

        if (_col.gameObject.TryGetComponent<IDamageable>(out var _damageable))
        {
            _damageable.ApplyDamage(Host, damage);
        }
    }

    private void OnDestroy()
    {
        AttackController.OnChangedWeapon -= OnWeaponChanged;
    }
}
